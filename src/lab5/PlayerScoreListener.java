package lab5;
/**
 * Interface for player score change listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface PlayerScoreListener {
	/**
	 * Updates player scores in view.
	 */
	void scoreChanged();
}
