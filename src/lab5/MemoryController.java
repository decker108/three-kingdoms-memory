package lab5;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Timer;
/**
 * The MemoryController class represents the MVC Controller-class. This class
 * listens for changes to the model and retransmits them to the view.
 * MemoryController also aggregates the model and view class objects. 
 * @author Ola Rende, Fan Ding
 */
public class MemoryController implements CardClickListener, ModelStateListener, PlayerScoreListener, ActivePlayerListener, GameOverListener, PlayerNameListener, RestartListener, StartGameListener {
	private boolean godmode;
	private MemoryModel model;
	private MemoryView view;
	private int step = 0; // 0=one card front-up, 1=two cards front-up, 2=two cards front-down
	
	public MemoryController(boolean godmode) {
		this.godmode = godmode;
		model = new MemoryModel(godmode);
		view = new MemoryView();
	}
	/**
	 * Initializes the view and model objects, starting the game.
	 */
	public void startGame() {
		view.addCardClickListener(this);
		view.addPlayerNameListener(this);
		view.addRestartListener(this);
		view.addStartGameListener(this);
		view.setBoard(model.getCards());
		model.addModelStateListener(this); 
		model.addPlayerScoreListener(this);
		model.addActivePlayerListener(this);
		model.addGameOverListener(this);	
	}
	/**
	 * Resets the view and model objects, resetting the game.
	 */
	private void restartGame() {
		view.showStartingPlayer(model.getActivePlayer().getName());
		model.initModel(godmode);
		view.updateScores(0, 0);
		step = 0;
	}

	/**
	 * Listens for card-button clicks in the view and retransmits these to  
	 * the model.
	 * @param theCard The card of the button that was clicked.
	 */
	public void clicked(Card theCard) {
		if (step == 0) {
			model.turn(step, theCard);
			step = 1;
		} else if (step == 1) {
			if (model.turn(step, theCard)) {
				step = 2;
				
				Action updateCardsAction = new AbstractAction() {
				    public void actionPerformed(ActionEvent e) {
				    	model.turn(step, null);
						step = 0;
				    }
				};
				Timer timer = new Timer(1000, updateCardsAction);
				timer.setRepeats(false);
				timer.start();
			}
		}
	}
	/**
	 * Listens for card-state changes in the model and retransmits these to  
	 * the view.
	 */
	public void stateChanged() {
		view.updateBoard();
	}
	/**
	 * Listens for score changes in the model and retransmits these to  
	 * the view. 
	 */
	public void scoreChanged() {
		view.updateScores(model.p1.getScore(), model.p2.getScore());
	}
	/**
	 * Listens for active player changes in the model and retransmits these to  
	 * the view. 
	 */
	public void activePlayerChanged() {
		view.updateActivePlayer(model.getActivePlayer().getName());
	}
	/**
	 * Checks game victory conditions in the model and retransmits these to  
	 * the view.
	 * @param theList The ArrayList containing the player highscores
	 */
	public void gameOver(ArrayList<Highscores> theList) {
		view.setHighScores(theList, model.p1, model.p2);
	}
	/**
	 * Listens for name inputs in the view and retransmits these to the player
	 * objects in the model.
	 * @param name1 Name of player 1
	 * @param name2 Name of player 2
	 */
	public void namesSet(String name1, String name2) {
		model.p1.setName(name1);
		model.p2.setName(name2);
	}
	/**
	 * Listens for game restart events in the view and manages them.
	 */
	public void newGame() {
		restartGame();
	}
	@Override
	public void startGameClicked() {
		view.showStartingPlayer(model.getActivePlayer().getName());
	}
}