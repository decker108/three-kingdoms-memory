package lab5;
/**
 * Interface for active player change listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface ActivePlayerListener {
	/**
	 * Changes active player in view.
	 */
	void activePlayerChanged();
}
