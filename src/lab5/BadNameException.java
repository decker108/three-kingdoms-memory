package lab5;
/**
 * This exception is thrown when a player inputs an invalid name.
 * @author Ola Rende, Fan Ding
 *
 */
public class BadNameException extends Exception {
	private String errorMessage;
	/**
	 * Constructor.
	 */
	public BadNameException() {
		super();
	}
	/**
	 * Constructor with error message string.
	 * @param errorMessage The message to be displayed.
	 */
	public BadNameException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	} 
	/**
	 * Returns the error message.
	 * @return String
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
}
