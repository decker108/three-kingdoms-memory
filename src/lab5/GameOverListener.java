package lab5;

import java.util.ArrayList;

/**
 * Interface for game ending input listeners.
 * @author Ola Rende, Fan Ding
 */
public interface GameOverListener {
	/**
	 * End the game.
	 */
	void gameOver(ArrayList<Highscores> list);
}
