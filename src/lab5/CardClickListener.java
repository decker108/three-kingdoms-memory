package lab5;
/**
 * Interface for card button-click listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface CardClickListener {
	/**
	 * Selects this card and turns it around.
	 * @param theCard Card object that was clicked.
	 */
	void clicked(Card theCard);
}
