package lab5;
/**
 * Main class of the Memory game
 * @author Ola Rende, Fan Ding
 *
 */
public class MemoryMain {
	private static boolean godmode;
	/**
	 * Starts game.
	 * @param args N/A
	 */
	public static void main(String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("godmode")) {
				godmode = true;
			}
		}
		MemoryController memoryController = new MemoryController(godmode);
		memoryController.startGame();
	}
}
