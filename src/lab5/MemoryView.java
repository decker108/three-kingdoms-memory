package lab5;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;
/**
 * Represents the data graphically and provides a GUI.
 * @author Ola Rende, Fan Ding
 */
public class MemoryView implements ActionListener {
	private JPanel cardPanel, scorePanel, statusPanel, menuPanel;
	private JPanel boardPanel, highscorePanel, mainmenuPanel;
	private JLabel p1NameLabel, p1ScoreLabel, p2NameLabel, p2ScoreLabel, statusMsg;
	private JTextField inputField1, inputField2;
	private JMenuBar menuBar;
	private JMenu menuFile, menuHelp;
	private JMenuItem menuItemNewGame, menuItemPause, menuItemExit, menuItemAbout;
	private JFrame mainFrame;
	private JTable scoreTable;
	private List<JButton> buttons = new ArrayList<JButton>();
	private List<Card> cards;
	private List<CardClickListener> cardClickListeners = new ArrayList<CardClickListener>();
	private List<PlayerNameListener> playerNameListeners = new ArrayList<PlayerNameListener>();
	private List<RestartListener> restartListeners = new ArrayList<RestartListener>();
	private List<StartGameListener> startGameListeners = new ArrayList<StartGameListener>();
	public final CardLayout mainCardLayout = new CardLayout();
	private Object[][] scoreData;// = {{"AAA","1"},{"BBB","3"},{"CCC","2"}};
	private String[] columnNames;
	private boolean firstGame;
	
	/**
	 * Constructs a GUI.
	 */
	public MemoryView() {
		//The "mainframe"
		mainFrame = new JFrame("MEMORY");
		mainFrame.setLayout(mainCardLayout);
		mainFrame.setSize(800,600);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setResizable(false);
		
		//The panel containing the Memory-cards
		boardPanel = new JPanel();
		boardPanel.setLayout(new BorderLayout());
		cardPanel = new JPanel();
		cardPanel.setLayout(new GridLayout(6,6));
		boardPanel.add(cardPanel, BorderLayout.CENTER);
		scorePanel = new JPanel();
		scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.Y_AXIS));
		p1NameLabel = new JLabel("???");
		scorePanel.add(p1NameLabel);
		p1ScoreLabel = new JLabel("0");
		scorePanel.add(p1ScoreLabel);
		p2NameLabel = new JLabel("???");
		scorePanel.add(p2NameLabel);
		p2ScoreLabel = new JLabel("0");
		scorePanel.add(p2ScoreLabel);
		boardPanel.add(scorePanel, BorderLayout.EAST);
		statusPanel = new JPanel();
		statusMsg = new JLabel("Let the game commence!");
		statusPanel.add(statusMsg);
		boardPanel.add(statusPanel, BorderLayout.SOUTH);
		mainFrame.getContentPane().add(boardPanel, "BOARD");
		
		//The panel containing the highscore table
		highscorePanel = new JPanel();
		scoreData = new Object[10][2];
		mainFrame.getContentPane().add(highscorePanel, "HIGHSCORE");
		
		//The panel containing the main menu
		mainmenuPanel = new JPanel();
		mainmenuPanel.setLayout(new BorderLayout());
		menuPanel = new JPanel();
		inputField1 = new JTextField();
		inputField2 = new JTextField();
		menuPanel = getMainMenuPanel();
		mainmenuPanel.add(menuPanel, BorderLayout.CENTER);
		mainFrame.getContentPane().add(mainmenuPanel, "MAINMENU");
		
		mainFrame.setJMenuBar(setupMenu());
		
		mainCardLayout.show(mainFrame.getContentPane(), "MAINMENU");
		mainFrame.setVisible(true);
	}
	/**
	 * Sets up the menubar and it's contents, returns a completed menubar.
	 * @return JMenuBar The finished menubar.
	 */
	private JMenuBar setupMenu() {
		menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		menuHelp = new JMenu("Help");
		menuItemNewGame = new JMenuItem("New Game");
		menuItemPause = new JMenuItem("Pause");
		menuItemExit = new JMenuItem("Exit");
		menuItemAbout = new JMenuItem("About");
		menuItemExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menuItemAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Memory" + '\n' + "Av Ola Rende, Fan Ding" + '\n' + "2010");
			}
		});
		menuItemPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Paused. Press OK to resume.");
			}
		});
		menuItemNewGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainCardLayout.show(mainFrame.getContentPane(), "BOARD");
				for (RestartListener restartListener : restartListeners) {
					restartListener.newGame();
				}
			}
		});
		menuFile.add(menuItemNewGame);
		menuFile.add(menuItemPause);
		menuFile.add(menuItemExit);
		menuHelp.add(menuItemAbout);
		menuBar.add(menuFile);
		menuBar.add(menuHelp);
		return menuBar;
	}
	/**
	 * Generates a main menu JPanel containing two JTextFields and and a JButton.
	 * @return The finished JPanel
	 */
	private JPanel getMainMenuPanel() {
		JPanel jPanel = null;
		if (jPanel  == null) {
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.gridx = 1;
			gridBagConstraints5.gridy = 2;
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.gridx = 1;
			gridBagConstraints4.fill = GridBagConstraints.BOTH;
			gridBagConstraints4.weightx = 1.0D;
			gridBagConstraints4.weighty = 1.0D;
			gridBagConstraints4.gridy = 3;
			JLabel filling = new JLabel();
			filling.setText(" ");
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints3.gridy = 1;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.gridx = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.insets = new Insets(0, 0, 0, 4);
			gridBagConstraints2.anchor = GridBagConstraints.NORTH;
			gridBagConstraints2.gridy = 1;
			JLabel player2Label = new JLabel();
			player2Label.setText("Name of player 2:");
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.weightx = 1.0;
			gridBagConstraints1.gridx = 1;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.insets = new Insets(0, 0, 0, 4);
			gridBagConstraints.gridy = 0;
			JLabel player1Label = new JLabel();
			player1Label.setText("Name of player 1:");
			jPanel = new JPanel();
			jPanel.setLayout(new GridBagLayout());
			jPanel.setSize(new Dimension(290, 114));
			jPanel.add(player1Label, gridBagConstraints);
			//JTextField inputField1 = new JTextField();
			jPanel.add(inputField1, gridBagConstraints1);
			jPanel.add(player2Label, gridBagConstraints2);
			//JTextField inputField2 = new JTextField();
			jPanel.add(inputField2 , gridBagConstraints3);
			JButton jButton = new JButton("Start game");
			jPanel.add(filling, gridBagConstraints4);
			jPanel.add(jButton, gridBagConstraints5);
			jButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Boolean namesAreOK = false;
					String text = inputField1.getText();
					String text2 = inputField2.getText();
					try {
						if (text.length() == 0 || text2.length() == 0) {
							throw new BadNameException("Error: Invalid name(s), blank name(s) not allowed!");
						} else {
							namesAreOK = true;
						}
					} catch (BadNameException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, e1.getErrorMessage());
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(null, e2.getMessage());
					}
					if (namesAreOK == true) {
						for (PlayerNameListener playerNameListener : playerNameListeners) {
							playerNameListener.namesSet(text, text2);
						}
						p1NameLabel.setText(text);
						p2NameLabel.setText(text2);
						mainCardLayout.show(mainFrame.getContentPane(), "BOARD");
						for (StartGameListener startGameListener : startGameListeners) {
							startGameListener.startGameClicked();
						}
					}
				}
			});
		}
		return jPanel;
	}
	
	/**
	 * Sets up the memory cards with the corresponding buttons.
	 * @param cards
	 */
	public void setBoard(List<Card> cards) {
		this.cards = cards;
		for (int i = 0; i < cards.size(); i++) {
			JButton button = new JButton();
			buttons.add(button);
			button.addActionListener(this);
			cardPanel.add(button);
		}
		updateBoard();
		mainFrame.pack();
	}
	/**
	 * Updates the cards based on changes to the model state.
	 */
	public void updateBoard() {
		Icon rearIcon = new ImageIcon(MemoryView.class.getResource("/images/rear.jpg"));
		for (int i = 0; i < cards.size(); i++) {
			Card card = cards.get(i);
			JButton button = buttons.get(i);
			if (card.getState() == CardState.REAR) {
				button.setIcon(rearIcon);
				button.setVisible(true);
			} else if (card.getState() == CardState.FRONT) {
				String filename = "/images/front" + card.getPictureIndex() + ".jpg";
				button.setIcon(new ImageIcon(MemoryView.class.getResource(filename)));
				button.setVisible(true);
			} else {
				button.setVisible(false);
			}	
		}
	}
	/**
	 * Updates the GUI representation of the player scores.
	 * @param a Score for player 1
	 * @param b Score for player 2
	 */
	public void updateScores(int a, int b) {
		p1ScoreLabel.setText("" + a);
		p2ScoreLabel.setText("" + b);
	}
	/**
	 * Displays the player name of the starting player.
	 * @param input
	 */
	public void showStartingPlayer(String input) {
		JOptionPane.showMessageDialog(null, "" + input + " gets the first turn.");
	}
	/**
	 * Displays the winning player(s) and score(s).
	 * @param theList The ArrayList containing player highscores
	 * @param p1 Player 1
	 * @param p2 Player 2
	 */
	public void setHighScores(ArrayList<Highscores> theList, Player p1, Player p2) {
		if (firstGame == false) {
			for (int i = 0; i < theList.size(); i++) {
				String playerName = theList.get(i).getPlayerName();
				int score = theList.get(i).getScore();
				scoreData[i][0] = playerName;
				scoreData[i][1] = score;
			}
			columnNames = new String[] {"Name","Score"};
			scoreTable = new JTable(scoreData, columnNames); //(data, colNames)
			JScrollPane scrollPane = new JScrollPane(scoreTable);
			highscorePanel.add(scrollPane, BorderLayout.NORTH);
			mainFrame.getContentPane().add(highscorePanel, "HIGHSCORE");
			firstGame = true;
		} else {
			TableModel temp = scoreTable.getModel();
			for (int i = 0; i < theList.size(); i++) {
				temp.setValueAt(theList.get(i).getPlayerName(), i, 0);
				temp.setValueAt(theList.get(i).getScore(), i, 1);
			}
			scoreTable.setModel(temp);
		}
		mainCardLayout.show(mainFrame.getContentPane(), "HIGHSCORE");
		
		if (p1.getScore() > p2.getScore()) {
			JOptionPane.showMessageDialog(null, "" + p1.getName() + " won!");
		} else if (p1.getScore() < p2.getScore()) {
			JOptionPane.showMessageDialog(null, "" + p2.getName() + " won!");
		} else {
			JOptionPane.showMessageDialog(null, "It's a tie!?");
		}
	}

	/**
	 * Handles card button click events.
	 * @param e The event given by the event dispatch thread.
	 */
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		int i = buttons.indexOf(o);
		Card clickedCard = null;
		clickedCard = cards.get(i);
		for (CardClickListener listener : cardClickListeners) {
			listener.clicked(clickedCard);
		}
	}
	/**
	 * Updates the label displaying the current player's name.
	 * @param playerName
	 */
	public void updateActivePlayer(String playerName) {
		statusMsg.setText(playerName + ", it's your turn!");
	}
	/**
	 * Adds a card click listener.
	 * @param listener
	 * @see CardClickListener
	 */
	public void addCardClickListener(CardClickListener listener) {
		cardClickListeners.add(listener);
	}
	/**
	 * Adds a player name listener.
	 * @param playerNameListener
	 * @see PlayerNameListener
	 */
	public void addPlayerNameListener(PlayerNameListener playerNameListener) {
		playerNameListeners.add(playerNameListener);
	}
	/**
	 * Adds a restart listener.
	 * @param restartListener
	 * @see RestartListener
	 */
	public void addRestartListener(RestartListener restartListener) {
		restartListeners.add(restartListener);
	}

	public void addStartGameListener(StartGameListener startGameListener) {
		startGameListeners.add(startGameListener);
	}
}
