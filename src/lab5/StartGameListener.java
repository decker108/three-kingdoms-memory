package lab5;
/**
 * Interface for game start listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface StartGameListener {
	/**
	 * Switch from main menu to board in view.
	 */
	void startGameClicked();
}
