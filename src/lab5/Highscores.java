package lab5;

import java.io.Serializable;

/**
 * Score for one player.
 * @author Ola Rende, Fan Ding
 */
public class Highscores implements Comparable<Highscores>, Serializable{
	private static final long serialVersionUID = 1L;
	private String playerName;
	private int score;
	/**
	 * Constructor.
	 * @param playerName
	 * @param score
	 */
	public Highscores(String playerName, int score) {
		super();
		this.playerName = playerName;
		this.score = score;
	}
	/**
	 * Returns player name.
	 * @return String
	 */
	public String getPlayerName() {
		return playerName;
	}
	/**
	 * Sets player name.
	 * @param playerName
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	/**
	 * Returns the score
	 * @return Int
	 */
	public int getScore() {
		return score;
	}
	/**
	 * Sets the score.
	 * @param score
	 */
	public void setScore(int score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return new String("Name: " + this.playerName + " Score:" + this.score);
	}
	@Override
	public int compareTo(Highscores other) {
		return this.score - other.getScore();
	}
}
