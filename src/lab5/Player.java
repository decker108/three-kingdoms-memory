package lab5;
/**
 * Represents a player participating in the game.
 * @author Ola Rende, Fan Ding
 */
public class Player {
	private String name;
	private int score;
	private boolean myTurn;
	
	/**
	 *Player constructor.
	 */
	public Player() {
		init();
	}
	
	public void init() {
		score = 0;
		myTurn = false;
	}
	/**
	 * Returns player name.
	 * @return String
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets player name.
	 * @param name String containing name.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Returns player score.
	 * @return int
	 */
	public int getScore() {
		return score;
	}
	/**
	 * Sets player score.
	 * @param score Value to be set as score.
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * Returns player turn.
	 * @return boolean
	 */
	public boolean isMyTurn() {
		return myTurn;
	}
	/**
	 * Sets player turn.
	 * @param myTurn Boolean setting the players turn.
	 */
	public void setMyTurn(boolean myTurn) {
		this.myTurn = myTurn;
	}
}
