package lab5;

/**
 * Interface for modelstate change listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface ModelStateListener {
	/**
	 * Interface method to be contacted by the MemoryModel objects.
	 */
	void stateChanged();
}
