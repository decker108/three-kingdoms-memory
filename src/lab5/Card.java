package lab5;
/**
 * Objects of the Card-class represent a memory card.
 * @author Ola Rende, Fan Ding
 */
public class Card {
	private int index;
	private CardState state = CardState.REAR;
	private int pictureIndex;
	/**
	 * Constructor.
	 * @param index The number of the card.
	 * @param pictureIndex The index of the card's front picture. Corresponds
	 * to the picture's filename.
	 */
	public Card(int index, int pictureIndex) {
		this.index = index;
		this.pictureIndex = pictureIndex;
	}
	/**
	 * Returns number of the card.
	 * @return int
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * Returns state of the card.
	 * @return Cardstate-enum
	 */
	public CardState getState() {
		return state;
	}
	/**
	 * Sets the Cardstate-enum of the card.
	 * @param state Cardstate-enum of the desired state.
	 */
	public void setState(CardState state) {
		this.state = state;
	}
	/**
	 * Returns picture index of the card.
	 * @return int
	 */
	public int getPictureIndex() {
		return pictureIndex;
	}
	/**
	 * Sets the picture index of the card.
	 * @param pictureIndex Picture index int of the card.
	 */
	public void setPictureIndex(int pictureIndex) {
		this.pictureIndex = pictureIndex;
	}
}
