package lab5;

/**
 * Interface for game restart listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface RestartListener {
	/**
	 * Restarts game.
	 */
	void newGame();
}
