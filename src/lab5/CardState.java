package lab5;
/**
 * Contains the different states cards can have: REAR, FRONT & REMOVED. Rear 
 * means the back of the card should be visible, front means the face of 
 * the card should be visible and removed means the card has been paired with 
 * another and taken away.
 * @author Ola Rende, Fan Ding
 */
public enum CardState {
	REAR, FRONT, REMOVED
}
