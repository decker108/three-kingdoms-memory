package lab5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;
/**
 * Stores and manipulates the data. Model aggregates the cards and the players.
 * Also contained are state change listeners.
 * @author Ola Rende, Fan Ding
 */
public class MemoryModel {
	public Player p1, p2;
	public List<Player> hiscoreList = new ArrayList<Player>();
	private List<ModelStateListener> modelStateListeners = new ArrayList<ModelStateListener>();
	private List<PlayerScoreListener> playerScoreListeners = new ArrayList<PlayerScoreListener>();
	private List<ActivePlayerListener> activePlayerListeners = new ArrayList<ActivePlayerListener>();
	private List<GameOverListener> gameOverListeners = new ArrayList<GameOverListener>();
	private List<Card> cards = new ArrayList<Card>();
	private Card firstCard, secondCard;
	private boolean isStep1Match;
	private Random randGen = new Random();
	private List<Highscores> highscoreList = new ArrayList<Highscores>();
	//private String filename = "highscore.ser";
	private File hiscoreFile; 
	/**
	 * Constructs the cards and players. Initializes the model.
	 * @param godmode 
	 */
	public MemoryModel(boolean godmode) {
		String homeDir = System.getProperty("user.home");
		File hiscoreDir = new File(homeDir, "3k-memory");
		hiscoreDir.mkdirs();
		hiscoreFile = new File(hiscoreDir, "highscore.ser");
		System.out.println(hiscoreFile);
		for (int i = 0; i < 36; i++) {
			cards.add(new Card(i,i/2));
		}
		p1 = new Player();
		p2 = new Player();
		initModel(godmode);
		try {
			deSerializeFromFile(hiscoreFile);
		} catch (IOException e) {
			System.out.println("File not found!");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found!");
			e.printStackTrace();
		}
	}
	/**
	 * Initializes the model, resetting data.
	 * @param godmode 
	 */
	public void initModel(boolean godmode) {
		for (Card card : cards) {
			card.setState(CardState.REAR);
		}
		p1.init();
		p2.init();
		if (godmode == false) {
			shuffleCards(); //DEBUG, kommentera tillbaka om bortkommenterad!
		}
		chooseStartingPlayer();
		isStep1Match = false;
		fireStateChanged();
	}
	/**
	 * Returns the list of cards.
	 * @return List< Card >
	 */
	public List<Card> getCards() {
		return cards;
	}
	/**
	 * The turn method controls the main game mechanism in memory:
	 * turning cards and comparing them. The method also check victory 
	 * conditions, changes player turns, updates score and signals for view 
	 * updates and visual model state changes.
	 * @param step The current step in the card drawing process.
	 * @param currentCard The card currently being turned.
	 * @return True if card could be turned, else false.
	 */
	public boolean turn(int step, Card currentCard) {
		if (step == 0) {
			firstCard = currentCard;
			currentCard.setState(CardState.FRONT);
		} else if (step == 1) {
			if (firstCard != currentCard) {
				isStep1Match = firstCard.getPictureIndex() == currentCard.getPictureIndex();
				currentCard.setState(CardState.FRONT);
				secondCard = currentCard;
			} else {
				return false;
			}
		} else {
			if (isStep1Match) {
				increaseScore();
				firstCard.setState(CardState.REMOVED);
				secondCard.setState(CardState.REMOVED);
				//nextPlayersTurn(); //Should lucky streaks be allowed?
			} else {
				firstCard.setState(CardState.REAR);
				secondCard.setState(CardState.REAR);
				nextPlayersTurn();
			}
			for (ActivePlayerListener activePlayerListener : activePlayerListeners) {
				activePlayerListener.activePlayerChanged();
			}
			if (checkForGameOver() == true) {
				highscoreList.add(new Highscores(p1.getName(),p1.getScore()));
				highscoreList.add(new Highscores(p2.getName(),p2.getScore()));
				Collections.sort(highscoreList);
				Collections.reverse(highscoreList);
				while (highscoreList.size() > 10) {
					highscoreList.remove(highscoreList.size()-1);
				}
				for (GameOverListener gameOverListener : gameOverListeners) {
					gameOverListener.gameOver((ArrayList<Highscores>) highscoreList);
				}
				try {
					serializeToFile(hiscoreFile);
				} catch (IOException e) {
					System.out.println("File could not be created!");
					e.printStackTrace();
				}
			}
		}
		fireStateChanged();
		return true;
	}
	/**
	 * Signal model state change to controller.
	 */
	private void fireStateChanged() {
		for (ModelStateListener modelStateListener : modelStateListeners) {
			modelStateListener.stateChanged();
		}
	}
	/**
	 * Checks for game end condition: Are there any non-removed cards left?
	 * @return Returns true if all cards have been removed, else false.
	 */
	private boolean checkForGameOver() {
		int i = 0;
		for (Card card : cards) {
			if (card.getState() == CardState.REMOVED) {
				i++;
			}
		}
		if (i == cards.size()) {
			return true;
		}
		return false;
	}
	/**
	 * Randomly selects the starting player. 
	 */
	public void chooseStartingPlayer() { 
		if (randGen.nextBoolean() == true) {
			 p1.setMyTurn(true);
		 } else {
			 p2.setMyTurn(true);
		 }
	}
	/**
	 * Switches turn to the next non-active player.
	 */
	public void nextPlayersTurn() {
		if (p1.isMyTurn() == true) {
			p1.setMyTurn(false);
			p2.setMyTurn(true);
		} else {
			p1.setMyTurn(true);
			p2.setMyTurn(false);
		}
	}
	/**
	 * Checks for currently active player.
	 * @return The currently active player.
	 */
	public Player getActivePlayer() {
		if (p1.isMyTurn()) {
			return p1;
		} else {
			return p2;
		}
	}
	/**
	 * Randomizes card positions.
	 */
	public void shuffleCards() {
		Random random = new Random();
		int pos1, pos2, noOfShuffles = 108, temp; 
		for(int i = 0; i < noOfShuffles; i++) {
			pos1 = random.nextInt(cards.size()); 
			pos2 = random.nextInt(cards.size()); 
			temp = cards.get(pos1).getPictureIndex(); 
			cards.get(pos1).setPictureIndex(cards.get(pos2).getPictureIndex()); 
			cards.get(pos2).setPictureIndex(temp); 
		}
	}
	/**
	 * Increases active player's score.
	 */
	public void increaseScore() {
		getActivePlayer().setScore(getActivePlayer().getScore() + 1);
		for (PlayerScoreListener playerScoreListener : playerScoreListeners) {
			playerScoreListener.scoreChanged();
		}
	}
	
	public void serializeToFile(File hiscoreFile) throws IOException {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(hiscoreFile));
			out.writeObject(highscoreList);
			
			System.out.println("Serializing successfully completed");
		} catch(FileNotFoundException e0) {
			JOptionPane.showMessageDialog(null, "File not found" + " " + e0.getStackTrace().toString());
		} catch(Exception e) {
			System.out.println(e);
		} finally {
			try {
				if(out != null) out.close();
			} catch(IOException e) {
				System.out.println(e);
			}
		}
	}
	
	public void deSerializeFromFile(File hiscoreFile) throws IOException, ClassNotFoundException {
		FileInputStream fin = null;
		try {
			fin = new FileInputStream(hiscoreFile);
			ObjectInputStream ois = new ObjectInputStream(fin);
			highscoreList = (ArrayList<Highscores>) ois.readObject();
			System.out.println("Deserialize done!");
		} catch(ClassNotFoundException e1) {
			System.out.println("The object can not be found");
		} catch(FileNotFoundException e2) {
			JOptionPane.showMessageDialog(null, "Serialfile not found.");
		} catch(IOException e) {
			System.out.println(e);
		} finally {
			try {
				if(fin != null) fin.close();
			} catch(IOException e){}
		}
	}
	/**
	 * Adds a model state listener.
	 * @param modelStateListener
	 * @see ModelStateListener
	 */
	public void addModelStateListener(ModelStateListener modelStateListener) {
		modelStateListeners.add(modelStateListener);		
	}
	/**
	 * Adds a player score listener.
	 * @param playerScoreListener
	 * @see PlayerScoreListener
	 */
	public void addPlayerScoreListener(PlayerScoreListener playerScoreListener) {
		playerScoreListeners.add(playerScoreListener);
	}
	/**
	 * Adds a active player listener.
	 * @param activePlayerListener
	 * @see ActivePlayerListener
	 */
	public void addActivePlayerListener(ActivePlayerListener activePlayerListener) {
		activePlayerListeners.add(activePlayerListener);
	}
	/**
	 * Adds a game over listener.
	 * @param gameOverListener
	 * @see GameOverListener
	 */
	public void addGameOverListener(GameOverListener gameOverListener) {
		gameOverListeners.add(gameOverListener);
	}
}
