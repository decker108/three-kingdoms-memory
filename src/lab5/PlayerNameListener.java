package lab5;

/**
 * Interface for playername input listeners.
 * @author Ola Rende, Fan Ding
 *
 */
public interface PlayerNameListener {
	/**
	 * When the names have been input, set player names in the MemoryModel class
	 * @param name1 name of player 1
	 * @param name2 name of player 2
	 */
	void namesSet(String name1, String name2);
}
